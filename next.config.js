/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env:{
  
    BACKEND:"https://e-social-media-api.herokuapp.com"
  }
}
  // BACKEND:"http://localhost:8030"
module.exports = nextConfig
