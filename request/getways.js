

export default {
    common: process.env.BACKEND + "/api",
    auth: process.env.BACKEND + "/api/auth",
}