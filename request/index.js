
import axios from "axios"
import gateways from "./getways"
import urlJoin from "url-join"
import Cookie from "js-cookie"




const agent = () => {
    const token = Cookie.get("token")
    const instance = axios.create({
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        }
    });

    if (token && token != "") {
        instance.defaults.headers.Authorization = token
    }

    
    instance.interceptors.response.use(function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    }, function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        if (error?.response.status == 403) {
            Cookie.remove("session")
            Cookie.remove("token")
            location.href = "/auth/signin"
        }
        return Promise.reject(error);
    });
    return instance
}


export const requestUtil = async ({ method, gateway, path, body = "", params = "", headers = {} }) => {

    const endpoint = urlJoin(gateways[gateway], path)

    const endpointUtil = {
        get: agent().get,
        post: agent().post,
        del: agent().delete,
        put: agent().put,
    }


    try {
        var response;
        if (method == "get") {
            response = await endpointUtil[method](endpoint, { params })
        }
        else {
            response = await endpointUtil[method](endpoint, body, { params })
        }
        return response?.data
    } catch (error) {
        console.log(error)
        return Promise.reject(error?.response?.data || error)
    }
}


export const request = {
    get: (gateway, path, options) => requestUtil({ method: "get", path, params: options?.params, gateway }),
    post: (gateway, path, options) => requestUtil({ method: "post", path, params: options?.params, body: options?.body, gateway }),
    del: (gateway, path, options) => requestUtil({ method: "del", path, params: options?.params, body: options?.body, gateway }),
    put: (gateway, path, options) => requestUtil({ method: "put", path, params: options?.params, body: options?.body, gateway }),
}