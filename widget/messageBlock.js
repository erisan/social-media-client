
import { Avatar } from '@mui/material'
import React, { useEffect, useRef } from 'react'
import { formatName } from '../utils'

export default function MessageBlock({ showAvatar, receiverProfile, createdAt, children, owner, message }) {

  const block = useRef("")

  useEffect(() => {
    if (block.current) {
      block.current.scrollIntoView()
    }
  }, [block])

  if (!owner) {
    return (
      <div className='yCenter' ref={block}>
        {showAvatar ?
      <Avatar style={{background :"orange"}} alt={formatName(receiverProfile?.fullname)}>{formatName(receiverProfile?.fullname)}</Avatar>
    :
    <div style={{width: "40px", height: "40px"}}>
      </div>    
    }
      <div  className={`ms-2 yCenter chat-receiver-message-block d-inline-block ${showAvatar ? "" : "no-left-rad"}`} >
        <div>
        {children}
        </div>
      </div>
      </div>
    )
  }
  return (
    <div className='w-100 text-end'>
      <div className={`chat-sender-message-block d-inline-block`}>
        {children}
      </div>
      <div className='yCenter justify-content-end'>
        <div style={{ fontSize: "12px" }}>
          {/* <svg
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g clipPath="url(#clip0_1_594)">
              <path
                d="M9.0004 16.17L4.8304 12L3.4104 13.41L9.0004 19L21.0004 7L19.5904 5.59L9.0004 16.17Z"
                fill="#27BE67"
              />
            </g>
            <defs>
              <clipPath id="clip0_1_594">
                <rect width={24} height={24} fill="white" />
              </clipPath>
            </defs>
          </svg> */}

        </div>

      </div>
    </div>
  )
}
