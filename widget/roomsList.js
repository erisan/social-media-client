import { Avatar } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { request } from '../request'

export default function RoomList({ currentRoom, current, data, currentUserId, ...props }) {
  const [details, setDetails] = useState(null)

  const secondMemberId = data.members.find(member => member !== currentUserId)

  useEffect(() => {
    if (data) {
      const getUser = async () => {
        try {
          const response = await request.get("common", `/user/${secondMemberId}`)
          setDetails(response.data)

        } catch (error) {
          console.log(error, '===')
        }

      }

      getUser()
    }
  }, [data])

  return (
    <div className={`d-flex py-3 p-3 ${current ? "current" : ""} room-container`} onClick={() => props.onClick(details)}>
      <div className='me-2'>
        <Avatar alt="Remy Sharp" src="https://images.pexels.com/photos/7365339/pexels-photo-7365339.jpeg?auto=compress&cs=tinysrgb&w=400">ER</Avatar>
      </div>
      <div className='w-100'>
        <div className='w-100 yCenter justify-content-between'>
          <div className='profile-name float-start d-inline-block'>
            <div className='yCenter'>
              <div className='fw-bold' style={{fontSize: "14px"}}>
                  {details?.fullname}
              </div>
            
              <div className='ms-2 text-lowercase' style={{color: "#6E767D" , fontSize: "14px"}}>
                @{details?.username}
              </div>
            </div>


          </div>
          <div className='float-end yCenter d-inline-block' style={{fontSize:"10px", color: "#D9D9D9"}}>
            Dec 25
          </div>

        </div>
        <div className='clearfix'></div>
        <div className='font15 w-100'>
          <div className='d-inline-block room-last-message' style={{color: "#6E767D"}}>
          {data?.lastMessage}
          </div>
       
          <svg
          className='d-inline-block float-end'
          style={{width: "20px" , height: "auto"}}
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M9 16.17L4.83 12L3.41 13.41L9 19L21 6.99997L19.59 5.58997L9 16.17Z"
              fill="#27BE67"
            />
          </svg>

        </div>
      </div>
    </div>
  )
}
