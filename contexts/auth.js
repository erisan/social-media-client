
import React, { createContext, useContext, useEffect, useReducer } from "react";
import authReducer, { logout, persistAuth } from "../reducers/auth";
import Cookie from "js-cookie"
import { request } from "../request";

const AuthContext = createContext()



const AuthContextProvider = ({ children }) => {

    const [auth, dispatchAuth] = useReducer(authReducer, { isLoading: true, accessToDashboard: false, user: {} })
      
    useEffect(() => {
        (async () => {

            try {
                const currentUser = await getSession()
                if (currentUser) {
                    dispatchAuth({ type: persistAuth, data: { user: currentUser, isLoading: false, accessToDashboard: true } })

                }
                else {
                    dispatchAuth({ type: logout })

                }
            } catch (error) {
                dispatchAuth({ type: logout })
            }

        })()

    }, [])

    const saveSession = (data) => {
        Cookie.set("session", JSON.stringify(data))
    }


    const getSession = async () => {
        const data = Cookie.get("session")
        const token = Cookie.get("token")
        if (!token) return null;
        if (data) {
            return JSON.parse(data)
        }
        else if (token) {
            console.log("kskskskskksksk" , token , "----")
            const user = await request.get("common", "/user/me")
            saveSession(user.data)
            return user.data;
        }
        else {
            return null
        }


    }

    const setToken = (token) => {
        Cookie.set("token" , token)
    }

    const confirmSignin = async (token) => {
        setToken(token)
        const user = await getSession()
        location.href = "/"
    }

    return (
        <AuthContext.Provider value={{ ...auth, dispatchAuth, confirmSignin }} >
            {children}
        </AuthContext.Provider>
    )
}



export const useAuth = () => {
    const value = useContext(AuthContext)
    return value;
}

export default AuthContextProvider


