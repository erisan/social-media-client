import React from 'react'

export default function AuthLayout({children}) {
    return (
        <div className='w-100 container-fluid'>
            <div className='row'>
                <div className='col-md-6'>

                </div>

                <div className='col-md-6'>
                 {children}
                </div>
            </div>

        </div>
    )
}
