import React, { Children, useEffect } from 'react'
import { useAuth } from '../../contexts/auth'
import Sidebar from './sidebar'

export default function DashboardLayout({ children }) {

    const {isLoading , accessToDashboard} =useAuth()
    const auth = useAuth()

    console.log(isLoading , "--------" , "auth"  , auth)
    if(isLoading){
        return "Loading"
    }

   useEffect(()=>{
    if(!accessToDashboard && !isLoading){
        location.href = "/auth/signin"
    }

   }, [accessToDashboard , isLoading])
    return (
        
        <div className='container-fluid w-100  layout-wrapper overflow-hidden' style={{ height: "100vh" }}>
            <div className='row'>
                <div className='col-md-3 overflow-hidden' style={{maxWidth: "20%"}}>
                    <Sidebar />
                </div>
                <div className='col-md px-0 overflow-hidden' >
                    {
                        children
                    }
                </div>


            </div>

        </div>
    )
}
