import { ListItemButton, ListItemText } from '@mui/material'
import React from 'react'
import sidebar from '../../data/sidebar'
import Link from "next/link"

export default function Sidebar() {


    return (
        <div className='w-100 h-100 ps-4 mt-5 sidebar-container' style={{ overflow: "auto" }}>
            {
                sidebar.map((data, _index) => {
                    return (

                        <Link key={_index} href={data.to}>
                        <a className='py-3 d-block'>
                        <div className='yCenter'>
                            <div className='me-3'>

                                {data?.icon}

                            </div>
                            <div>
                                {data?.title}
                            </div>
                        </div>
                        </a>
                        </Link>


                    )
                })
            }

        </div>
    )
}
