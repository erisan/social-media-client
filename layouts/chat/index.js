import React, { useEffect, useState } from 'react'
import { request } from '../../request'
import ChatPlayground from './chatPlayground'
import ChatSidebar from './chatSidebar'

export default function ChatLayout({ setAllMessages, allMessages, rooms: roomsData, socket, receiversocketId, currentUserId }) {
    const [currentRoom, setCurrentRoom] = useState(null)
    const [receiverProfile , setReceiverProfile] = useState(null)
    const [rooms , setRooms] = useState(roomsData)

    useEffect(()=>{
     setRooms(roomsData)
    },[roomsData])
    const secondMemberId = currentRoom?.members.find(member => member !== currentUserId)

     useEffect(()=>{
     const getMessages = async ()=>{
      try {
        const response = await request.get("common" ,`/messages/${currentRoom._id}`)

        setAllMessages(response.data)
        
      } catch (error) {
        console.log(error , "this is error")
      }
     }

     if(currentRoom){
        getMessages()
     }
   
     },[currentRoom])

    const sendMessage = async (message = "This is a message") => {

        try {
            const updatedData = rooms.map((room)=>{
                if(room._id == currentRoom._id){
                    room = {...room , lastMessage: message}
                }
                return room;

            })
            setRooms(updatedData)
            // setCurrentRoom({...currentRoom , lastMessage: message})
            setAllMessages(prev => [...prev, {  sender: currentUserId,
                message,
                roomId: currentRoom._id }])

            if (socket) {
                // Dont forget to send to a particular socket id
                console.log(currentRoom.members[1], "=====")
                const receiverSocket = receiversocketId(secondMemberId)
                // If Receiver is online
                console.log("receiverSocket", receiverSocket)
                if (receiverSocket) {
                    // socket.to(receiverSocket).emit("sendMessage", { roomId: currentRoom?._id, sender: currentUserId, message })
                }
                socket.emit("sendMessage", { roomId: currentRoom?._id, sender: currentUserId, message, receiverSocketId: receiverSocket })
            }
            await request.post("common", "/messages", {
                body: {
                    sender: currentUserId,
                    message,
                    roomId: currentRoom._id
                }
            })
        }
        catch (error) {

        }
    }

    const handleChat = (room , receiverProfile) => {
        localStorage.setItem("currentRoomId" , room._id)
        setCurrentRoom(room)
        setReceiverProfile(receiverProfile)
    }
    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-4 px-0'>
                    <ChatSidebar currentRoom={currentRoom} currentUserId={currentUserId} onChat={handleChat} rooms={rooms} />
                </div>


                <div className='col-md text-white w-100 px-0'>
                    <div style={{overflowX: "hidden", overflowY: "auto" , height: "100vh"}}>
                    <ChatPlayground receiverProfile={receiverProfile} currentUserId={currentUserId} sendMessage={sendMessage} allMessages={allMessages} roomId={currentRoom?._id} />
                    </div>
                </div>

            </div>

        </div>
    )
}
