import React, { useState } from 'react'
import RoomList from '../../widget/roomsList'

export default function ChatSidebar({ currentRoom, currentUserId, rooms, onChat, ...props }) {
    const [value, setValue] = useState("")
    return (
        <div className='w-100 h-100 chat-sidebar-container' style={{ overflow: "auto" }}>
            <div className='yCenter px-3 justify-content-between py-3'>
                <div className='font18 ' style={{fontWeight: 700}}>
                    Messages
                </div>
                <div>
                    <svg
                    style={{width: "20px" , height:"auto"}}
                        width={27}
                        height={24}
                        viewBox="0 0 27 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M21.25 5.52344V6.62706L11.7042 13.1356C11.2794 13.4252 10.7206 13.4252 10.2958 13.1356L0.75 6.62706V5.52344C0.75 4.83308 1.30964 4.27344 2 4.27344H20C20.6904 4.27344 21.25 4.83308 21.25 5.52344ZM12.5492 14.3749L21.25 8.44254V21.5234C21.25 22.2138 20.6904 22.7734 20 22.7734H2C1.30964 22.7734 0.75 22.2138 0.75 21.5234V8.44254L9.45083 14.3749C10.3853 15.0121 11.6147 15.0121 12.5492 14.3749Z"
                            stroke="#27BE67"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                        <path
                            d="M22.1016 4.34375H25.3203V5.42188H22.1016V8.75H21.0156V5.42188H17.8125V4.34375H21.0156V1H22.1016V4.34375Z"
                            fill="#27BE67"
                        />
                        <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M26.3203 3.34375V6.42188H23.1016V9.75H20.0156V6.42188H16.8125V3.34375H20.0156V0H23.1016V3.34375H26.3203ZM21.0156 4.34375H17.8125V5.42188H21.0156V8.75H22.1016V5.42188H25.3203V4.34375H22.1016V1H21.0156V4.34375Z"
                            fill="black"
                        />
                    </svg>

                </div>

            </div>
            <div className='px-3 py-3' style={{ borderTop: "1px solid #292929", borderBottom: "1px solid #292929" }}>
                <input style={{fontSize: "12px"}} value={value} onChange={(e) => setValue(e.target.value)} placeholder='Search for people or groups' className='input w-100 p-2 ps-4' />

            </div>

            {
                rooms?.map((_room, _index) => {
                    console.log(_room, "_room_room_room")
                    return (
                        <RoomList current={currentRoom?._id == _room._id} currentUserId={currentUserId} onClick={(receiverProfile) => onChat(_room, receiverProfile)} data={_room} key={_index} />
                    )
                })

            }
        </div>
    )
}
