import { TextField } from '@mui/material'
import React, { useRef, useState } from 'react'
import MessageBlock from '../../widget/messageBlock'

export default function ChatPlayground({ receiverProfile, allMessages, roomId, sendMessage, currentUserId }) {
  const [value, setValue] = useState("")


  const handleSubmit = (e) => {
    e.preventDefault()
    sendMessage(value)
    setValue("")
  }

  const header = () => {
    return (
      <div className='w-100 chat-playground-header  px-3' style={{paddingTop: "6.5px", paddingBottom: "6.5px"}}>
        <div className='float-start'>
          <div className='font18'>
            {
              receiverProfile?.fullname
            }
          </div>
          <div className='font14'>
            @{
              receiverProfile?.username
            }
          </div>
        </div>

        <div className='float-end'>

        </div>

      </div>
    )
  }

  const footer = () => {
    return (
      <div className='w-100 px-3  py-2 yCenter chat-playground-footer'>
        <div className='yCenter'>
          <svg
            width={24}
            height={24}
            style={{ height: "20px", width: "auto" }}
            className="me-3"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              x="0.75"
              y="0.75"
              width="22.5"
              height="22.5"
              rx="1.25"
              stroke="#27BE67"
              strokeWidth="1.5"
            />
            <path
              d="M0.666656 22L7.99999 14.6667L10.8889 17.3333M17.3333 23.3333L10.8889 17.3333M10.8889 17.3333L16.6667 11.3333L23.3333 16.6667"
              stroke="#27BE67"
              strokeWidth="1.5"
            />
            <circle cx="8.66666" cy="8.66666" r={2} fill="#27BE67" />
          </svg>
          <svg
            width={26}
            height={26}
            style={{ height: "20px", width: "auto" }}

            viewBox="0 0 26 26"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              x={1}
              y={1}
              width={24}
              height={24}
              rx={2}
              stroke="#27BE67"
              strokeWidth="1.5"
            />
            <path
              d="M20.8667 9.19469H16.6667C16.5933 9.19469 16.5333 9.25899 16.5333 9.33758V16.5892C16.5333 16.6678 16.5933 16.7321 16.6667 16.7321H17.6533C17.7267 16.7321 17.7867 16.6678 17.7867 16.5892V13.676H20.59C20.6633 13.676 20.7233 13.6117 20.7233 13.5332V12.6955C20.7233 12.6169 20.6633 12.5526 20.59 12.5526H17.7867V10.3592H20.8667C20.94 10.3592 21 10.2949 21 10.2163V9.33758C21 9.25899 20.94 9.19469 20.8667 9.19469ZM14.9333 9.21255H14C13.9267 9.21255 13.8667 9.27685 13.8667 9.35544V16.6071C13.8667 16.6856 13.9267 16.7499 14 16.7499H14.9333C15.0067 16.7499 15.0667 16.6856 15.0667 16.6071V9.35544C15.0667 9.27685 15.0067 9.21255 14.9333 9.21255ZM12.6667 12.8008H9.97501C9.90168 12.8008 9.84168 12.8651 9.84168 12.9437V13.7243C9.84168 13.8029 9.90168 13.8672 9.97501 13.8672H11.5567L11.5517 14.0261C11.5317 15.0764 10.7917 15.7854 9.70334 15.7854C8.43334 15.7854 7.63834 14.7191 7.63834 12.9866C7.63834 11.2755 8.41834 10.2146 9.66334 10.2146C10.5767 10.2146 11.215 10.695 11.4717 11.5613H12.7417C12.515 10.0038 11.3083 9 9.66334 9C7.63334 8.99822 6.33334 10.5557 6.33334 12.9973C6.33334 15.4675 7.61834 17 9.68334 17C11.585 17 12.8 15.7372 12.8 13.7564V12.9437C12.8 12.8651 12.74 12.8008 12.6667 12.8008Z"
              fill="#27BE67"
            />
          </svg>
        </div>

        <form onSubmit={handleSubmit} className='w-100 px-4'>
          <input value={value} onChange={(e) => setValue(e.target.value)} placeholder='Start your message' className='input w-100 p-2 ps-4' />
          <button className='d-none' type='submit'></button>
        </form >
        <div className='yCenter'>
          <svg
            width={21}
            height={22}
            viewBox="0 0 21 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M6 11L1 1L19.3333 11M6 11H19.3333M6 11L1 21L19.3333 11"
              stroke="#27BE67"
              strokeOpacity="0.6"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>

        </div>

      </div>
    )
  }
  return (
    <div className=' chat-playground my-5 overflow-hidden'>
      <div className='my-5 overflow-hidden'>
      {
        roomId ?
          <>
            {
              header()
            }
            {
              allMessages && allMessages.map((message , index) => {
                return (
                  <div className='px-3'>
                    <h5 className='text-white'>
                      <MessageBlock senderNoCurve={message?.sender == currentUserId && message?.sender == allMessages[index+1 == allMessages.length ? index : index+1].sender} showAvatar={message.sender !== currentUserId && message.sender !== allMessages[index+1].sender } {...message} receiverProfile={receiverProfile} owner={message.sender == currentUserId}>
                        {message.message}
                      </MessageBlock>
                    </h5>

                  </div>
                )
              })
            }

            {
              footer()
            }
          </>

          : 
          <div className='text-center d-flex' style={{width: "100%" , height: "100vh" , alignItems: "center" , justifyContent: "center"}}>
<div> <iframe src="https://giphy.com/embed/LiF9FQHuQ7owp6qxj9" width="80" height="80" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p>
  </p>
Select friend to begin conversation.
            </div>
            </div>
      }


</div>
    </div>
  )
}
