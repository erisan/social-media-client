var sidebars;

export default sidebars = [
    {
        title: "Home",
        icon: <svg
            width={26}
            height={23}
            viewBox="0 0 26 23"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M3.6087 6.21739L5.46423 20.1339C5.59671 21.1275 6.44428 21.8696 7.44668 21.8696H18.0884C19.066 21.8696 19.9004 21.1627 20.0611 20.1984L22.3913 6.21739M3.6087 6.21739L13 1L22.3913 6.21739M3.6087 6.21739L1 7.78261M22.3913 6.21739L25 7.78261"
                stroke="#D9D9D9"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <circle
                cx="13.0001"
                cy="11.9565"
                r="2.65217"
                stroke="#D9D9D9"
                strokeWidth={2}
            />
        </svg>,
        to: "/"

    },
    {
        title: "Notification",
        icon: <svg
            width={27}
            height={26}
            viewBox="0 0 27 26"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M1.35878 19.5869C1.24606 19.6242 1.12725 19.6636 1 19.7181H8.62956H18.0198H25.6494C23.5323 19.7181 23.4121 18.9875 22.8103 15.3304C22.4779 13.3101 21.9985 10.3966 20.9542 6.21975C19.6654 1.06433 8.62956 -2.58308 5.10823 6.80677C2.97882 19.0506 2.45902 19.2227 1.35878 19.5869Z"
                fill="#27BE67"
            />
            <path
                d="M8.62956 19.7181H1C1.12725 19.6636 1.24606 19.6242 1.35878 19.5869C2.45902 19.2227 2.97882 19.0506 5.10823 6.80677C8.62956 -2.58308 19.6654 1.06433 20.9542 6.21975C21.9985 10.3966 22.4779 13.3101 22.8103 15.3304C23.4121 18.9875 23.5323 19.7181 25.6494 19.7181H18.0198M8.62956 19.7181C8.62956 21.4787 10.0381 25 13.3247 25C16.6113 25 17.6285 21.4787 18.0198 19.7181M8.62956 19.7181H18.0198"
                stroke="#27BE67"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
        ,
        to: "/notification"

    },

    {
        title: "Messages",
        icon: <svg
            width={24}
            height={22}
            viewBox="0 0 24 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M1 2C1 1.44772 1.44771 1 2 1H22C22.5523 1 23 1.44771 23 2V3.81818V19.8182C23 20.3705 22.5523 20.8182 22 20.8182H2C1.44771 20.8182 1 20.3705 1 19.8182V3.81818V2Z"
                stroke="#D9D9D9"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>

        ,
        to: "/messages"

    }

]