import React, { useEffect, useId, useRef, useState } from 'react'
import { useAuth } from '../../contexts/auth';
import DashboardLayout from '../../layouts/dashboard';
import ChatLayout from '../../layouts/chat'
import { request } from '../../request';
const { io } = require("socket.io-client");

export default function index() {
  const socket = useRef("")
  const [onlineUsers, setOnlineUsers] = useState([])
  const [rooms, setRooms] = useState(null)
  const [allMessages, setAllMessages] = useState([])

  const { user } = useAuth()




  console.log("user", user)
  useEffect(() => {
    if (!socket.current) {
      socket.current = io("http://localhost:4000")
    }
  }, [onlineUsers])

  useEffect(() => {
    if (socket && user?._id) {
      socket.current.emit("addUser", { userId: user?._id })

      socket.current.on("onlineUsers", connectedUsers => {
        setOnlineUsers(connectedUsers)
      })

      // socket.current.emit("disconnected" , socket.current?.id)

      socket.current.on("disconnect", d => {
        socket.current.emit("user-disconnected", { userId: user?._id })
      })


    }


  }, [socket, user])

  useEffect(() => {
    socket.current.on("receiveMessage", message => {
      setAllMessages(prev => [...prev, message])
      const updatedData = rooms?.map((room)=>{
        if(room._id == message.roomId){
            room = {...room , lastMessage: message.message}
        }
        return room;
    
    })
    setRooms(updatedData)
    })


  }, [socket])

  useEffect(() => {

    const getRooms = async () => {
      try {
        const response = await request.get("common", "/rooms")
        console.log(response, "response")
        setRooms(response?.data)
      } catch (error) {

      }
    }

    getRooms()

  }, [])


  console.log(allMessages, "................")

  const getSocketIdByUser = (userId) => {

    if (onlineUsers.length > 0) {
      const socketId = onlineUsers.find(user => user.userId == userId)
      return socketId?.socketId
    }


  }


  console.log("onlineUsers", onlineUsers)
  return (
    <DashboardLayout>
      <ChatLayout setAllMessages={setAllMessages} allMessages={allMessages} receiversocketId={getSocketIdByUser} currentUserId={user?._id} socket={socket.current} rooms={rooms} >

      </ChatLayout>
    </DashboardLayout>
  )
}
