import "bootstrap/dist/css/bootstrap.css";
import "../layouts/layout.scss"
import "../styles/utils.scss"

import '../styles/globals.scss'
import AuthContextProvider from "../contexts/auth";

function MyApp({ Component, pageProps }) {
  return (
    <AuthContextProvider>
      <Component {...pageProps} />
    </AuthContextProvider>

  )
}

export default MyApp
