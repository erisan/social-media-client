import { TextField } from '@mui/material'
import React, { useState } from 'react'
import ButtonComponent from '../../components/button'
import AuthLayout from '../../layouts/auth'
import { request } from '../../request'
import axios from "axios"
import Alert from '../../components/snackbar'

export default function Signin() {

    const [open, setOpen] = React.useState(false);
    const [message, setMessage] = useState(null)
    const [data, setData] = useState(false)

    const handleChange = (name, value) => {

        setData(prev => ({ ...prev, [name]: value }))

    }



    const handleSubmit = async (e) => {
        e.preventDefault()
        console.log(data , "[[[[]]-------aa")
        try {
            const response = await request.post("auth", "/signup", {
                body: {
                    ...data
                }
            })

        } catch (error) {
            setOpen(true)
            setMessage(error?.msg)
        }

    }
    return (
        <AuthLayout>
            <form onSubmit={handleSubmit}>
                <TextField required onChange={(e) => handleChange("fullname", e.target.value)} label="Fullnamea" id="outlined-basic" variant="outlined" />
                <br />
                <br />
                <TextField required onChange={(e) => handleChange("username", e.target.value)} id="outlined-basic" label="Username" variant="outlined" />
                <br />
                <br />
                <TextField required type={"email"} onChange={(e) => handleChange("email", e.target.value)} id="outlined-basic" label="Email" variant="outlined" />
                <br />
                <br />
                <TextField required onChange={(e) => handleChange("password", e.target.value)} type="password" id="outlined-basic" label="Password" variant="outlined" />
                <br />
                <br />

                <ButtonComponent type="submit" label={"Signup"} />
            </form>

            <Alert open={open} message={message} close={() => setOpen(false)} />

        </AuthLayout>
    )
}
