import { TextField } from '@mui/material'
import React, { useState } from 'react'
import ButtonComponent from '../../components/button'
import AuthLayout from '../../layouts/auth'
import { request } from '../../request'
import axios from "axios"
import Alert from '../../components/snackbar'
import { useAuth } from '../../contexts/auth'
import Cookie from "js-cookie"

export default function Signin() {

    const [open, setOpen] = React.useState(false);
    const [message, setMessage] = useState(null)
    const [data, setData] = useState(false)

    const { confirmSignin } = useAuth()

    const handleChange = (name, value) => {
        setData(prev => ({ ...prev, [name]: value }))
    }



    const handleSubmit = async (e) => {
        Cookie.remove("token")
        Cookie.remove("session")
        e.preventDefault()

        try {
            const response = await request.post("auth", "/signin", {
                body: {
                    ...data
                }
            })
            if (response?.token) {
                confirmSignin(response?.token)
            }

        } catch (error) {
            setOpen(true)
            setMessage(error?.msg)
        }

    }
    return (
        <AuthLayout>
            <form onSubmit={handleSubmit}>
                <TextField required type={"email"} onChange={(e) => handleChange("email", e.target.value)} id="outlined-basic" label="Email" variant="outlined" />
                <br />
                <br />
                <TextField required onChange={(e) => handleChange("password", e.target.value)} type="password" id="outlined-basic" label="Password" variant="outlined" />
                <br />
                <br />

                <ButtonComponent type="submit" label={"Signin"} />
            </form>

            <Alert open={open} message={message} close={() => setOpen(false)} />

        </AuthLayout>
    )
}
