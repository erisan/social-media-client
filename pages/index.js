import { Avatar, Button, Skeleton } from '@mui/material'
import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import DashboardLayout from '../layouts/dashboard'
import { request } from '../request'
import styles from '../styles/Home.module.css'

export default function Home() {
  const [suggession, setSuggestion] = useState(null)


  const handleFollow = async (data)=>{
      try {

        const response = await request.put("common" , "/user/follow" , {body: {userId: data._id}})
        
      } catch (error) {
        
      }
  }

  const suggestFriend = async () => {
    try {
      const response = await request.get("common", "/user/who/suggest")
      setSuggestion(response.data)
      console.log("response.data" , response.data)

    } catch (error) {
      console.log(error , "error")
    }
  }
  useEffect(() => {
    suggestFriend()
  }, [])
  return (
    <DashboardLayout>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-8'>

          </div>
          <div className='col-md'>
            <div>
              <h3 className='text-white'>
                Who to follow
              </h3>
            </div>
            {
              !suggession ? [1, 2, 3, 4].map(() =>
                <div className='yCenter mb-3'>
                  <div style={{ minWidth: "60px" }}>
                    <Skeleton
                      sx={{ bgcolor: 'grey.900' }}
                      variant="circular"
                      width={50}
                      height={50}
                    />
                  </div>
                  <div className='w-100 mt-3'>
                    <Skeleton
                      sx={{ bgcolor: 'grey.900' }}
                      variant="rectangular"
                      width={"70%"}
                      height={20}
                    />

                  </div>


                </div>
              )
                :
                suggession && suggession.map((data, index) => {
                  return (
                    <div className='yCenter'>
                      <div className='text-white'>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                        <div>
                          <div>
                            {data?.fullname                                                                                                                                                                                                      }
                          </div>
                          <div>
                            @{data.username}
                          </div>
                        </div>
                      </div>
                      <div>
                        <Button onClick={()=>handleFollow(data)} variant="outlined">Outlined</Button>
                      </div>
                    </div>
                  )
                })
            }


          </div>
        </div>

      </div>
    </DashboardLayout>
  )
}
