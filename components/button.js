import React from 'react'
import Button from '@mui/material/Button';

export default function ButtonComponent({label,...props}) {
  return (
<Button variant="contained" {...props}>{label}</Button>
  )
}
