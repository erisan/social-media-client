

export const formatName = (name) => {
    if (!name) return null;
    const fullname = name.split(" ")
    if (fullname.length == 1) return fullname[0].charAt(0) + fullname[0].charAt(1)
    return fullname[0].charAt(0) + fullname[1].charAt(1)
}